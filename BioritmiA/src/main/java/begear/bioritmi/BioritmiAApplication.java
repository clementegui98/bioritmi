package begear.bioritmi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BioritmiAApplication {

	public static void main(String[] args) {
		SpringApplication.run(BioritmiAApplication.class, args);
	}

}
